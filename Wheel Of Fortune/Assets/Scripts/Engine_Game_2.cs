﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/// <summary>
/// Engine.cs is responsible for setting up the game, responding to player input,
/// and deciding if the game is won or lost.
/// @author Tiffany Fisher, 9-10-2015
/// </summary>

//Force unity to add the references script if it is not already on the object
[RequireComponent(typeof(References_Game_2))]
public class Engine_Game_2 : MonoBehaviour
{

    //Variables
    References_Game_2 refs;

    string question = "Who is known as Alexander the Great?"; //Stores the question the player should guess
    string answer1 = "Alexander, Correct";                    //Stores the first answer provided to the player
    string answer2 = "Julius";                                //Stores the second answer for the player
    string answer3 = "Ronnie";                                //Stores the thrid answer for the player
    string answer4 = "Jimmy Johnson";                         //Stores the fourth answer for the player

    int numQuestions = 0;
    int numSent;
    public int correctAnswers;

    /// <summary>
    /// Called by References once it is done gathering all neccessary information
    /// Starts the game
    /// </summary>
    void Begin()
    {
        //Gets a link to the references script
        refs = gameObject.GetComponent<References_Game_2>();
        //Starts the game
        StartRound();
    }


    /// <summary>
    /// Sets up a single question to be answered
    /// </summary>
    public void StartRound()
    {
        numQuestions++;
        //Retrieve number of questions and display it to the screen
        numSent = refs.questions.Count;
        refs.score.text = System.Convert.ToString(numQuestions + "/" + numSent);

        //Pick a new question
        int randomSent = Random.Range(0, numSent);

        //Retrieve data for that question
        question = refs.questions[randomSent];
        answer1 = refs.answer1[randomSent];
        answer2 = refs.answer2[randomSent];
        answer3 = refs.answer3[randomSent];
        answer4 = refs.answer4[randomSent];

        //Display the answers
        if(answer1.Contains(","))
        {
            string[] parsedAnswer = answer1.Split(',');
            refs.buttons[0].GetComponentInChildren<Text>().text = parsedAnswer[0];
        }
        else
            refs.buttons[0].GetComponentInChildren<Text>().text = answer1;

        if (answer2.Contains(","))
        {
            string[] parsedAnswer = answer2.Split(',');
            refs.buttons[1].GetComponentInChildren<Text>().text = parsedAnswer[0];
        }
        else
            refs.buttons[1].GetComponentInChildren<Text>().text = answer2;

        if (answer3.Contains(","))
        {
            string[] parsedAnswer = answer3.Split(',');
            refs.buttons[2].GetComponentInChildren<Text>().text = parsedAnswer[0];
        }
        else
            refs.buttons[2].GetComponentInChildren<Text>().text = answer3;

        if (answer4.Contains(","))
        {
            string[] parsedAnswer = answer4.Split(',');
            refs.buttons[3].GetComponentInChildren<Text>().text = parsedAnswer[0];
        }
        else
            refs.buttons[3].GetComponentInChildren<Text>().text = answer4;

        //Display the question
        refs.question.text = question;

        //Enable all of the buttons on the screen so the player can interact again
        foreach (Button bt in refs.buttons)
        {
            bt.interactable = true;
        }
        
    }

    public void _CheckCorrect(Text playerAnswer)
    {
        string[] answers = { answer1, answer2, answer3, answer4 };

        foreach (string answer in answers)
        {
            string[] parsedAnswer = answer.Split(',');
            if (playerAnswer.text == parsedAnswer[0])
                correctAnswers++;
        }

        StartRound();
    }

    private void Update()
    {
        if(numQuestions > numSent)
        {
            foreach (Button bt in refs.buttons)
                bt.interactable = false;
            refs.score.text = System.Convert.ToString(numSent + "/" + numSent);
        }

    }
}
