﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(References))]
public class LoadingScript : MonoBehaviour {

    FileInfo originalFile;
    TextAsset textFile;
    TextReader textReader;

    public List<string> sentences = new List<string>();
    public List<string> clues = new List<string>();

	// Use this for initialization
	void Start ()
    {
        textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
        textReader = new StringReader(textFile.text);

        string lineOfText;
        int lineNumber = 0;

        while ((lineOfText = textReader.ReadLine()) != null)
        {
            if(lineNumber%2==0)
            {
                sentences.Add(lineOfText);
            }

            else
            {
                clues.Add(lineOfText);
            }

            lineNumber++;
        }

        SendMessage("Gather");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
