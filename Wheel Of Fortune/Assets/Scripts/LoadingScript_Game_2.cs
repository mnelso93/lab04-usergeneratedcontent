﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(References_Game_2))]
public class LoadingScript_Game_2 : MonoBehaviour {


    FileInfo originalFile;
    TextAsset textFile;
    TextReader textReader;

    public List<string> questions = new List<string>();
    public List<string> answer1 = new List<string>();
    public List<string> answer2 = new List<string>();
    public List<string> answer3 = new List<string>();
    public List<string> answer4 = new List<string>();

    // Use this for initialization
    void Start()
    {
        if (questions.Count == 0 && answer1.Count == 0 && answer2.Count == 0 && answer3.Count == 0 && answer4.Count == 0)
        {
            textFile = (TextAsset)Resources.Load("embedded_Game_2", typeof(TextAsset));
            textReader = new StringReader(textFile.text);

            string lineOfText;
            int lineNumber = 0;

            while ((lineOfText = textReader.ReadLine()) != null)
            {
                if (lineNumber % 5 == 0)
                {
                    questions.Add(lineOfText);
                }

                else if (lineNumber % 5 == 1)
                {
                    answer1.Add(lineOfText);
                }

                else if (lineNumber % 5 == 2)
                {
                    answer2.Add(lineOfText);
                }

                else if (lineNumber % 5 == 3)
                {
                    answer3.Add(lineOfText);
                }

                else if (lineNumber % 5 == 4)
                {
                    answer4.Add(lineOfText);
                }

                lineNumber++;
            }
        }

        SendMessage("Gather");
    }
}
