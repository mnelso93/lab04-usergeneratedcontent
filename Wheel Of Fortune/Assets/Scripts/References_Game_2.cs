﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
/// <summary>
/// References.cs gatheres references from various objects in the screen.
/// It then stores the references for use in the game at later points. 
/// @author Tiffany Fisher, 9-10-2015
/// </summary>

public class References_Game_2 : MonoBehaviour
{

    //Variables to store all information, hidden from designer
    //public Text hint;
    [HideInInspector]
    public Text question;
    [HideInInspector]
    public Text score;
    [HideInInspector]
    public Button[] buttons;
    [HideInInspector]
    public List<string> questions = new List<string>();
    [HideInInspector]
    public List<string> answer1 = new List<string>();
    [HideInInspector]
    public List<string> answer2 = new List<string>();
    [HideInInspector]
    public List<string> answer3 = new List<string>();
    [HideInInspector]
    public List<string> answer4 = new List<string>();

    /// <summary>
    /// Retrieve all the data from the LoadingScript
    /// </summary>
    void Gather()
    {
        questions = GetComponent<LoadingScript_Game_2>().questions;
        answer1 = GetComponent<LoadingScript_Game_2>().answer1;
        answer2 = GetComponent<LoadingScript_Game_2>().answer2;
        answer3 = GetComponent<LoadingScript_Game_2>().answer3;
        answer4 = GetComponent<LoadingScript_Game_2>().answer4;
    }

    //Altered
    void Start()
    {

        //Gather references to both hint and score location
        score = GameObject.Find("Score").GetComponent<Text>();

        //Gather references to all of the buttons that corrispond with answers
        buttons = GameObject.Find("Answers").GetComponentsInChildren<Button>();

        //Gather reference to the question location 
        question = GameObject.Find("Question").GetComponent<Text>();

        //Inform the Engine to begin the game!
        SendMessage("Begin");
    }


}
